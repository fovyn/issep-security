﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiSecurity.Models;
using WebApiSecurity.Models.Entities;
using WebApiSecurity.Services;
using PasswordEncoder = BCrypt.Net.BCrypt;

namespace WebApiSecurity.Controllers;

[Authorize]
[ApiController]
public class SecurityController : ControllerBase
{
    public JwtHelper JwtHelper { get; set; }
    public UserService UserService { get; set; }

    public SecurityController(JwtHelper jwtHelper, UserService userService)
    {
        JwtHelper = jwtHelper;
        UserService = userService;
    }

    [AllowAnonymous]
    [HttpPost("api/sign-in")]
    public IActionResult SignIn([FromBody] SignInDto dto)
    {
        IActionResult result = Unauthorized();
        if (ModelState.IsValid)
        {
            User? user = UserService.FindByUsername(dto.Username);
            if (user != null && !PasswordEncoder.Verify(dto.Password, user.Password))
            {
                result = Unauthorized();
            }
            else
            {
                string token = JwtHelper.GenerateToken(user);
                result = Ok(new { token });
            }
        }

        return result;
    }
    
    [Authorize]
    [HttpPost("api/register")]
    public IActionResult Register([FromBody] User user)
    {
        return Created("demo", UserService.Create(user));
    }
}