﻿using Microsoft.EntityFrameworkCore;
using WebApiSecurity.Models.Entities;

namespace WebApiSecurity;

public class IssepContext: DbContext
{
    public IConfiguration Configuration { get; set; }
    public DbSet<User> Users { get; set; }

    public IssepContext(IConfiguration configuration) : base()
    {
        Configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(Configuration.GetConnectionString("IssepContext"));
    }
}