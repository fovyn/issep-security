﻿using System.ComponentModel.DataAnnotations;

namespace WebApiSecurity.Models;

public class SignInDto
{
    [Required(ErrorMessage = "Le champ username est obligatoire")]
    public String Username { get; set; }
    
    [Required(ErrorMessage = "Le champ password est obligatoire")]
    public String Password { get; set; }
}