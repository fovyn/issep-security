﻿using WebApiSecurity.Models.Entities;

namespace WebApiSecurity.Repositories;

public class UserRepository
{
    private IssepContext Db { get; set; }
    public UserRepository(IssepContext context)
    {
        Db = context;
    }

    public IEnumerable<User> FindAll()
    {
        return Db.Users;
    }

    public User? FindByUsername(string username)
    {
        return Db.Users.FirstOrDefault(it => it.Username == username);
    }

    public User Insert(User user)
    {
        Db.Users.Add(user);

        Db.SaveChanges();

        return user;
    }
}