﻿using WebApiSecurity.Models.Entities;
using WebApiSecurity.Repositories;

using PasswordEncoder = BCrypt.Net.BCrypt;

namespace WebApiSecurity.Services;

public interface UserService
{
    IEnumerable<User> FindAll();
    User? FindByUsername(string username);

    User? Create(User user);
}

public class UserServiceImpl: UserService
{
    public UserRepository UserRepository { get; set; }

    public UserServiceImpl(UserRepository userRepository)
    {
        UserRepository = userRepository;
    }
    public IEnumerable<User> FindAll()
    {
        return UserRepository.FindAll();
    }

    public User? FindByUsername(string username)
    {
        return UserRepository.FindByUsername(username);
    }

    public User? Create(User user)
    {
        string hash = PasswordEncoder.HashPassword(user.Password);

        user.Password= hash;

        return UserRepository.Insert(user);
    }
}