﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using WebApiSecurity.Models.Entities;

namespace WebApiSecurity.Services;

public interface JwtHelper
{
    string GenerateToken(User user);
}

public class JwtHelperImpl: JwtHelper
{
    public IConfiguration Configuration { get; set; }

    public JwtHelperImpl(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public string GenerateToken(User user)
    {
        var configKey = Configuration["jwt:key"];
        var configIssuer = Configuration["jwt:issuer"];
        var configAudience = Configuration["jwt:audience"];

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configKey));
        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);   
        var token = new JwtSecurityToken(
            issuer: configIssuer,
            audience: configAudience,
            signingCredentials: credentials
        );

        return new JwtSecurityTokenHandler().WriteToken(token);
    }
}